<?php
/**
 * @file
 * vactory_testimonials.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function vactory_testimonials_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'testimonial';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Testimonial';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'slick';
  $handler->display->display_options['style_options']['slide_field_wrapper'] = 0;
  $handler->display->display_options['style_options']['cache'] = '';
  $handler->display->display_options['style_options']['mousewheel'] = 0;
  $handler->display->display_options['style_options']['optionset'] = 'x_grid';
  $handler->display->display_options['style_options']['override'] = 0;
  $handler->display->display_options['style_options']['overridables'] = array(
    'arrows' => 0,
    'autoplay' => 0,
    'dots' => 0,
    'draggable' => 0,
  );
  $handler->display->display_options['style_options']['slide_caption'] = array(
    'title' => 0,
    'body' => 0,
    'field_vactory_testimony_function' => 0,
    'field_vactory_testimony_city' => 0,
    'field_vactory_image' => 0,
  );
  $handler->display->display_options['style_options']['slide_overlay'] = '';
  $handler->display->display_options['style_options']['preserve_keys'] = 0;
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['type'] = 'text_trimmed';
  $handler->display->display_options['fields']['body']['settings'] = array(
    'trim_length' => '100',
  );
  /* Field: Content: Function */
  $handler->display->display_options['fields']['field_vactory_testimony_function']['id'] = 'field_vactory_testimony_function';
  $handler->display->display_options['fields']['field_vactory_testimony_function']['table'] = 'field_data_field_vactory_testimony_function';
  $handler->display->display_options['fields']['field_vactory_testimony_function']['field'] = 'field_vactory_testimony_function';
  $handler->display->display_options['fields']['field_vactory_testimony_function']['label'] = '';
  $handler->display->display_options['fields']['field_vactory_testimony_function']['element_label_colon'] = FALSE;
  /* Field: Content: City */
  $handler->display->display_options['fields']['field_vactory_testimony_city']['id'] = 'field_vactory_testimony_city';
  $handler->display->display_options['fields']['field_vactory_testimony_city']['table'] = 'field_data_field_vactory_testimony_city';
  $handler->display->display_options['fields']['field_vactory_testimony_city']['field'] = 'field_vactory_testimony_city';
  $handler->display->display_options['fields']['field_vactory_testimony_city']['label'] = '';
  $handler->display->display_options['fields']['field_vactory_testimony_city']['element_label_colon'] = FALSE;
  /* Field: Content: profileimage */
  $handler->display->display_options['fields']['field_vactory_image']['id'] = 'field_vactory_image';
  $handler->display->display_options['fields']['field_vactory_image']['table'] = 'field_data_field_vactory_image';
  $handler->display->display_options['fields']['field_vactory_image']['field'] = 'field_vactory_image';
  $handler->display->display_options['fields']['field_vactory_image']['label'] = '';
  $handler->display->display_options['fields']['field_vactory_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_vactory_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_vactory_image']['settings'] = array(
    'image_style' => 'testimonial',
    'image_link' => '',
  );
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'testimonials' => 'testimonials',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'slick';
  $handler->display->display_options['style_options']['slide_field_wrapper'] = 0;
  $handler->display->display_options['style_options']['cache'] = '';
  $handler->display->display_options['style_options']['mousewheel'] = 0;
  $handler->display->display_options['style_options']['optionset'] = 'clone_of_default';
  $handler->display->display_options['style_options']['override'] = 0;
  $handler->display->display_options['style_options']['overridables'] = array(
    'arrows' => 0,
    'autoplay' => 0,
    'dots' => 0,
    'draggable' => 0,
  );
  $handler->display->display_options['style_options']['slide_caption'] = array(
    'title' => 0,
    'body' => 0,
    'field_vactory_testimony_function' => 0,
    'field_vactory_testimony_city' => 0,
    'field_vactory_image' => 0,
  );
  $handler->display->display_options['style_options']['slide_overlay'] = '';
  $handler->display->display_options['style_options']['preserve_keys'] = 0;
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $export['testimonial'] = $view;

  return $export;
}
